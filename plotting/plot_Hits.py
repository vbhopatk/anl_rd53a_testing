import matplotlib.pyplot as plt
import json
import sys
import numpy as np

pixels = 192*400/3.

inputfile = sys.argv[1]
with open(inputfile, 'r') as f:
    text = f.read()
json_data = json.loads(text)
data = sum(json_data["Data"],[])
fig, ax = plt.subplots(figsize=(10,10),ncols=1, nrows=1)
n, bins, patches = plt.hist(data, bins=int(max(data))+2, range=[0,max(data)+2])
hits = 0
for i in range(int(max(data))):
    hits += n[i] * bins[i]
ax.text(max(data)/2, 40000, '{} hits/pixel'.format(hits/pixels))
plt.xlabel('Hits')
plt.show()
