from influxdb import InfluxDBClient
import datetime
import time
import serial
import os
import re
import subprocess

def read_info(module_temp, air_temp, air_humid):
    data_list = [{
        'measurement': 'RD53A-001-RealModule',
        'tags': {'cpu': 'dorothy'},
        'fields':{
            'time': datetime.datetime.now().strftime("%H:%M:%S"),
            'module_temp': float(module_temp),
            'air_temp': float(air_temp),
            'air_humid': float(air_humid)
        }
    }]
    return data_list

def IV_info(LV_voltage, LV_current, HV_voltage, HV_current):
    data_list = [{
        'measurement': 'RD53A-001-RealModule',
        'tags': {'cpu': 'dorothy'},
        'fields':{
            'time': datetime.datetime.now().strftime("%H:%M:%S"),
            'LV_voltage': float(LV_voltage),
            'LV_current': float(LV_current),
            'HV_voltage': float(HV_voltage),
            'HV_current': float(HV_current)
        }
    }]
    return data_list

client = InfluxDBClient(host='localhost',port=8086)
client.switch_database('dcsDB')
print('Please set threshold for interlock in the Arduino program!')

while True:
    ser = serial.Serial('/dev/ttyACM0',9600)
    b = ser.readline()
    string_n = b.decode()
    string = string_n.rstrip()
    print(string)
    try:
        air_temp, air_humid, module_temp = string.split()
        client.write_points(read_info(module_temp, air_temp, air_humid))
    except ValueError:
        print("Oooops! Arduino issue, not getting three numbers. Try again ...")
    time.sleep(3)

